package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/AntonZheleznov/jsonflat/flat"

	"github.com/jessevdk/go-flags"
)

type Options struct {
	InputPath  string `short:"i" long:"input" required:"true" description:"Input json file"`
	OutputPath string `short:"o" long:"output" required:"true" description:"Output json file"`
	flat.Config
}

func main() {
	var opts Options
	p := flags.NewParser(&opts, flags.None)
	if _, err := p.Parse(); err != nil {
		log.Fatal(err)
	}

	inputFile, err := os.Open(opts.InputPath)
	if err != nil {
		log.Fatal(fmt.Errorf("can't open input file: %w", err))
	}
	defer inputFile.Close()

	outputFile, err := os.OpenFile(opts.OutputPath, os.O_CREATE, os.ModePerm)
	if err != nil {
		log.Fatal(fmt.Errorf("can't create output file: %w", err))
	}
	defer outputFile.Close()

	bytes, err := ioutil.ReadAll(inputFile)
	if err != nil {
		log.Fatal(fmt.Errorf("can't read input file: %w", err))
	}

	var unstructured interface{}
	err = json.Unmarshal(bytes, &unstructured)
	if err != nil {
		log.Fatal(fmt.Errorf("invalid json: %w", err))
	}

	flatMap, err := flat.MakeFlat(unstructured, opts.Config)
	if err != nil {
		log.Fatal(fmt.Errorf("flat transformation error: %w", err))
	}

	encoder := json.NewEncoder(outputFile)
	encoder.SetIndent("", "    ")
	err = encoder.Encode(flatMap)
	if err != nil {
		log.Fatal(fmt.Errorf("can't write file results: %w", err))
	}

	log.Println("Successfully completed")
}
