package flat

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

var defaultConfig = Config{
	Delimiter:    ".",
	ExpandSlices: false,
}

func TestMakeFlat(t *testing.T) {
	// в тесте использую json input, т.к. в таком виде проще описывать вложенные структуры данных.
	testCases := []struct {
		Name         string
		InputJsonStr string // должно быть валидной json строкой
		Expected     map[string]interface{}
		Cfg          *Config
	}{
		{
			Name:         "Simple struct",
			InputJsonStr: `{ "Id": 0, "Name": "Jonh"}`,
			Expected: map[string]interface{}{
				"Id":   0.,
				"Name": "Jonh",
			},
			Cfg: &defaultConfig,
		},
		{
			Name:         "Embeded structure",
			InputJsonStr: `{ "Id": 0, "User": {"Name": "Jonh", "Surname": "Doe"}}`,
			Expected: map[string]interface{}{
				"Id":           0.,
				"User.Name":    "Jonh",
				"User.Surname": "Doe",
			},
			Cfg: &defaultConfig,
		},
		{
			Name:         "Don't expand slice",
			InputJsonStr: `{ "Id": 0, "SliceData": ["a", "b", "c"]}`,
			Expected: map[string]interface{}{
				"Id":        0.,
				"SliceData": []interface{}{"a", "b", "c"},
			},
			Cfg: &defaultConfig,
		},
		{
			Name:         "Expand slice",
			InputJsonStr: `{ "Id": 0, "SliceData": ["a", "b", "c"]}`,
			Expected: map[string]interface{}{
				"Id":          0.,
				"SliceData.0": "a",
				"SliceData.1": "b",
				"SliceData.2": "c",
			},
			Cfg: &Config{
				Delimiter:    ".",
				ExpandSlices: true,
			},
		},
		{
			Name:         "Expand slice of struct",
			InputJsonStr: `[{"Id":"A1", "Name": "Alex"}, {"Id":"B1", "Name": "Bert"}]`,
			Expected: map[string]interface{}{
				"0.Id":   "A1",
				"0.Name": "Alex",
				"1.Id":   "B1",
				"1.Name": "Bert",
			},
			Cfg: &Config{
				Delimiter:    ".",
				ExpandSlices: true,
			},
		},
		{
			Name:         "Empty struct return empty map",
			InputJsonStr: `{}`,
			Expected:     map[string]interface{}{},
			Cfg:          &defaultConfig,
		},
	}

	for _, test := range testCases {
		var unstructed interface{}
		err := json.Unmarshal([]byte(test.InputJsonStr), &unstructed)
		assert.NoError(t, err)

		actual, err := MakeFlat(unstructed, *test.Cfg)
		assert.Nil(t, err)
		assert.EqualValues(t, test.Expected, actual)
	}
}

func TestMakeFlatErrors(t *testing.T) {
	testCases := []struct {
		Name         string
		InputJsonStr string // должно быть валидной json строкой
		ExpectedErr  error
	}{
		{
			Name:         "Empty slice not allowed",
			InputJsonStr: `[]`,
			ExpectedErr:  ErrEmptySlicesNotAllowed,
		},
		{
			Name:         "Primitive types not allowed: number",
			InputJsonStr: `42`,
			ExpectedErr:  ErrInvalidTypeForRootElement,
		},
		{
			Name:         "Primitive types not allowed: strings",
			InputJsonStr: `"qwerty"`,
			ExpectedErr:  ErrInvalidTypeForRootElement,
		},
		{
			Name:         "Primitive types not allowed: null",
			InputJsonStr: `null`,
			ExpectedErr:  ErrInvalidTypeForRootElement,
		},
	}

	for _, test := range testCases {
		var unstructed interface{}
		err := json.Unmarshal([]byte(test.InputJsonStr), &unstructed)
		assert.NoError(t, err)

		_, err = MakeFlat(unstructed, defaultConfig)
		assert.ErrorIs(t, err, test.ExpectedErr)
	}
}
