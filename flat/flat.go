package flat

import (
	"fmt"
	"strconv"
	"sync"
)

func MakeFlat(source interface{}, cfg Config) (map[string]interface{}, error) {
	flatMap := make(map[string]interface{})

	err := validateRootElement(source)
	if err != nil {
		return flatMap, fmt.Errorf("JsonFlat validation error: %w", err)
	}

	var wg sync.WaitGroup
	foundLeafs := make(chan Leaf)

	wg.Add(1)
	go flatRec(true, &wg, foundLeafs, source, "", cfg)

	go func() {
		wg.Wait()
		close(foundLeafs)
	}()

	for leaf := range foundLeafs {
		flatMap[leaf.Key] = leaf.Value
	}

	return flatMap, nil
}

func validateRootElement(value interface{}) error {
	switch typedValue := value.(type) {
	case map[string]interface{}:
		return nil
	case []interface{}:
		if len(typedValue) == 0 {
			return ErrEmptySlicesNotAllowed
		}
	default:
		return ErrInvalidTypeForRootElement
	}

	return nil
}

func flatRec(isRoot bool, wg *sync.WaitGroup, ch chan Leaf, value interface{}, prefix string, cfg Config) {
	defer wg.Done()

	switch typedValue := value.(type) {
	case map[string]interface{}:
		for k, v := range typedValue {
			newKey := buildKey(isRoot, prefix, k, cfg.Delimiter)
			wg.Add(1)
			go flatRec(false, wg, ch, v, newKey, cfg)
		}
	case []interface{}:
		if cfg.ExpandSlices {
			expandSlice(isRoot, wg, ch, typedValue, prefix, cfg)
		} else {
			ch <- Leaf{
				Key:   prefix,
				Value: typedValue,
			}
		}
	default:
		ch <- Leaf{
			Key:   prefix,
			Value: value,
		}
	}
}

func expandSlice(isRoot bool, wg *sync.WaitGroup, ch chan Leaf, source []interface{}, prefix string, cfg Config) {
	for i, v := range source {
		newKey := buildKey(isRoot, prefix, strconv.Itoa(i), cfg.Delimiter)
		wg.Add(1)
		go flatRec(false, wg, ch, v, newKey, cfg)
	}
}

func buildKey(isRoot bool, prefix, subkey, delimiter string) string {
	if isRoot {
		return subkey
	}

	return prefix + delimiter + subkey
}
