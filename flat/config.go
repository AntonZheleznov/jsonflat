package flat

type Config struct {
	Delimiter    string `short:"d" long:"delimiter" default:"." description:"Delimeter for path parts"`
	ExpandSlices bool   `short:"e" long:"expand" description:"Expand slices"`
}
