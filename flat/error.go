package flat

import "errors"

var (
	ErrEmptySlicesNotAllowed     = errors.New("empty slices not allowed for flat transformation")
	ErrInvalidTypeForRootElement = errors.New("root element must be array or object")
)
