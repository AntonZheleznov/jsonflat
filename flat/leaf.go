package flat

type Leaf struct {
	Key   string
	Value interface{}
}
